#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
using namespace std;

int main() {
vector<string> v = {"a","b","c"};
auto s = accumulate(next(v.begin()), v.end(), v[0], [](const string& a, const string& b){return a + '-' + b;});
cout << s << endl;
return 0;
}
