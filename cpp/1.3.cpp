#include <iostream>
#include <string>
#include <map>
using namespace std;

void removeDuplicates(string s){
    if(s.length() < 2) return;

    int tail = 1;
    for(int i = 1; i < s.length(); i++){
        int j;
        for(j=0; j<tail; j++){
            if(s[i] == s[j]) break;
        }
        if(j == tail){
            s[tail] = s[i];
            ++tail;
        }    
    
    }
    s.resize(tail);
    cout << s << endl << tail<< endl;
}

bool anagrams(string s1, string s2){
    if(s1.length() != s2.length()) return false;

    std::map<char,int> mmap;

    for(int i = 0; i<s1.length(); i++){
        mmap[s1[i]]++;
        mmap[s2[i]]--;
    }
    for(std::map<char,int>::iterator it = mmap.begin(); it != mmap.end(); ++it){
        if(it->second != 0) return false;
    }
    return true;
}

int main(){

string s = "aaaaabbcd"; 

removeDuplicates(s);
cout << anagrams("ssa","aas") << endl;

return 0;
}
