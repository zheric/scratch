#include <iostream>
using namespace std;
/**
  Gives compile error: 
   A is a inacessable base of B
  B privately inherits A and therefore is not visable to outside world 
  More explaination here: 
  https://stackoverflow.com/questions/9661936/inheritance-a-is-an-inaccessible-base-of-b
*/
class A {

public:
    int a = 8;
};

class B : private A{
public:
    int b = 7;
};

void p(const A& a){
    cout << a.a << endl;
}

int main() {
    A b = B();
    p(b);
    return 0;
};
