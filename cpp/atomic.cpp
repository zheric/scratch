#include <iostream>
#include <thread>
#include <atomic>

class T {
public:
    T() { i_.store(0); }
    void inc() {
        for( int i = 0; i< 10; i++) {
            i_.fetch_add(1);
            std::cout << std::this_thread::get_id() << " : " << getI() << std::endl;
        }
    }
    void dec() {
        for( int i = 0; i< 10; i++) {
            i_.fetch_sub(1);
            std::cout << std::this_thread::get_id() << " : " << getI() << std::endl;
        } 
     } 
    std::thread tAdd() {
        return std::thread([=](){inc();});
    }
    std::thread tSub() {
        return std::thread([=](){dec();});
    }
    int getI() {
        return i_.load();
    }
private:
    std::atomic<int> i_;
};


int main() {
T t;
std::thread tAdd = t.tAdd();
std::thread tSub = t.tSub();

tAdd.join();
tSub.join();
std::cout << "Main: " << t.getI() << std::endl;
return 0;
}
