#import <Foundation/Foundation.h>

@interface SampleClass:NSObject
- (NSNumber *) multiplyA:(NSNumber *)a withB:(NSNumber*)b;
@end

@implementation SampleClass:NSObject
- (NSNumber *) multiplyA:(NSNumber *)a withB:(NSNumber*)b {
    float n1 = [a floatValue];
    float n2 = [b floatValue];
    float product = n1 * n2;
    NSNumber * res = [NSNumber numberWithFloat:product];
    return res;
}
@end

int main()
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    SampleClass * sampleClass = [[SampleClass alloc] init];
    NSNumber *a = [NSNumber numberWithFloat:10.5];
    NSNumber *b = [NSNumber numberWithFloat:10.0];
    NSNumber *res = [sampleClass multiplyA:a withB:b];
    NSString *rs = [res stringValue];
    NSLog(@"the product is : %@", rs);
    [pool drain];
    return 0;
}
