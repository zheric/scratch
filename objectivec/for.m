#import <Foundation/Foundation.h>

int main()
{
   const int  LENGTH = 10;
   const int  WIDTH  = 5;
   const char NEWLINE = '\n';
   int area;  
       
   area = LENGTH * WIDTH;
  for(int i = 1;i < area; i*=2) {
    NSLog(@"value of area : %d", i);
   }
   return 0;
}
