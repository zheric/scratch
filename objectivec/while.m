#import <Foundation/Foundation.h>

int main()
{
   const int  LENGTH = 10;
   const int  WIDTH  = 5;
   const char NEWLINE = '\n';
   int area;  
       
   area = LENGTH * WIDTH;
  while(area > 0) {
   NSLog(@"value of area : %d", area);
   area /= 2;
   }
   return 0;
}
